#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin

from .models import Tag, Projeto, Tramitacao
from .forms import ProjetoAddForm


class ProjetoAdmin(admin.ModelAdmin):
    list_display = ("nome", "apelido", "origem", "autoria",
                    "tag_list", "importante", "urgente", "arquivado",
                    "impacto", "apresentacao", "ultima_atualizacao")

    list_filter = ("importante", "urgente", "origem", "arquivado",
                   "impacto", "tags__nome")

    search_fields = ("nome", "apelido", "autoria", "origem", "local")

    initial_fields = ("origem", "id_site", "nome", "apelido", "tags",
                      "autoria", "apresentacao", "ementa", "local",
                      "apensadas", "importante", "urgente",
                      "arquivado", "impacto", "pontos_principais",
                      "informacoes_adicionais", "clippings",
                      "texto_acao")

    fields = initial_fields

    readonly_fields = ("nome", "autoria", "apresentacao", "ementa", "local",
                       "apensadas", "ultima_atualizacao")

    filter_horizontal = ("tags", )

    def get_form(self, request, obj=None, **kwargs):
        if obj is None:
            # Se obj é None, isso é a página de adição
            self.fields = ['origem', 'id_site', 'tags']
            return ProjetoAddForm
        else:
            # Precisamos disso aqui por que a instância sobrevive a várias
            # requisições. Sem resetar os campos eles não aparecem no form de
            # edição se o form de adição já foi usado.
            self.fields = self.initial_fields
            return super(ProjetoAdmin, self).get_form(request, obj=obj,
                                                      **kwargs)


class TramitacaoAdmin(admin.ModelAdmin):
    list_display = ("data", "projeto", "local", "descricao")
    list_filter = ("data", "local")
    search_fields = ("data", "projeto__nome", "local", "descricao")

    def has_add_permission(self, request):
        """
        Tramitações are created automatically when getting information for a
        Projeto, so they shouldn't be added directly through the admin
        interface.
        """
        return False


admin.site.register(Tag)
admin.site.register(Projeto, ProjetoAdmin)
admin.site.register(Tramitacao, TramitacaoAdmin)
