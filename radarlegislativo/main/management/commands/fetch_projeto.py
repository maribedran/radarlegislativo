#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import codecs

import yaml
from django.core.management.base import BaseCommand

from main.models import Projeto, Tag
from main.tasks import queue_download


TAGS_MAP = {
    'priv': u'Privacidade',
    'acesso': u'Acesso',
    'libex': u'Liberdade de expressão',
    'genero': u'Gênero',
    'inov': u'Inovação',
}

tags_list = []
for abbr, tag in TAGS_MAP.items():
    tags_list.append(u"{} - {}".format(abbr, tag))

tags_help_text = (u"Tags a serem adicionadas ao projeto (escolha entre as "
        u"seguintes: {})").format(", ".join(tags_list))


class Command(BaseCommand):
    help = u"Recupera informações de projetos da Câmara ou do Senado"

    def add_arguments(self, parser):
        parser.add_argument("--id", dest="id_site", type=int,
                help=u"Id do projeto no site do Senado ou da Câmara")
        parser.add_argument("--origem", dest="origem", type=str,
                choices=("camara", "senado"),
                help=u"A origem do projeto")
        parser.add_argument("--tags", dest="tags", nargs="+",
                help=tags_help_text)
        parser.add_argument("--from-file", dest="filename",
            help="Usa ids e tags e origem de um arquivo yaml")

    def load_yaml(self, filename):
        with codecs.open(filename, encoding='utf-8') as yaml_file:
            return yaml.load(yaml_file)

    def handle(self, *args, **options):
        if options["filename"]:
            data = self.load_yaml(options["filename"])
            for proj in data:
                tags = map(lambda t: TAGS_MAP.get(t), proj["tags"])
                tag_ids = list(Tag.objects.filter(nome__in=tags).values_list("id",
                        flat=True))

                if proj.get("id_senado"):
                    origem = Projeto.SENADO
                    id_no_site = proj["id_senado"]

                if proj.get("id_camara"):
                    origem = Projeto.CAMARA
                    id_no_site = proj["id_camara"]

                self.stdout.write((u"Enfileirando download dados do "
                        u"projeto {}").format(id_no_site))
                queue_download.delay(origem, id_no_site, tag_ids)

        else:

            if options.get("origem") is None:
                self.stderr.write(u"Para buscar dados de um projeto é "
                    u"necessário informar a origem.")

            if options["tags"]:
                tags = map(lambda t: TAGS_MAP.get(t), options["tags"])
                tag_ids = list(Tag.objects.filter(nome__in=tags).values_list("id",
                        flat=True))
            else:
                tag_ids = []

            if options["origem"] == "camara":
                origem = Projeto.CAMARA
            elif options["origem"] == "senado":
                origem = Projeto.SENADO

            id_no_site = options["id_site"]

            self.stdout.write((u"Enfileirando download dados do "
                    u"projeto {}").format(id_no_site))
            queue_download.delay(origem, id_no_site, tag_ids)
