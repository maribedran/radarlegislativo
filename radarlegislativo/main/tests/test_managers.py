# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import datetime
from django.test import TestCase, mock
from freezegun import freeze_time
from model_mommy import mommy

from main.models import Projeto


__all__ = ["TesteProjetoManager"]


@freeze_time('2018-04-05')
class TesteProjetoManager(TestCase):

    def setUp(self):
        self.pl_novo = mommy.make('Projeto')

        # Precisamos criar os objetos e depois mudar a data de
        # cadastro manualmente por que o auto_now_add sempre
        # sobrescreve o valor que definimos.
        self.pl_antigo = mommy.make('Projeto')
        self.pl_antigo.cadastro = datetime.date(2018, 3, 31)
        self.pl_antigo.save()

    def teste_encontra_o_PL_do_dia_05_na_mesma_semana_que_o_dia_06(self):
        self.assertIn(
            self.pl_novo,
            Projeto.objects.na_mesma_semana_que_o_dia(
                datetime.date(2018, 4, 6)),
        )

    def teste_nao_encontra_o_PL_do_dia_31_na_mesma_semana_que_o_dia_06(self):
        self.assertNotIn(
            self.pl_antigo,
            Projeto.objects.na_mesma_semana_que_o_dia(
                datetime.date(2018, 4, 6)),
        )

    @mock.patch('main.models.ProjetoManager.na_mesma_semana_que_o_dia')
    def teste_novos_PLs_inclui_PLs_da_ultima_semana(self, filtro_mockado):
        Projeto.objects.novos()
        filtro_mockado.assert_called_once_with(datetime.date.today())
