#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms

from .models import Projeto, Tag
from .fetcher import FetcherError
from .fetcher import fetch_camara_project, scrape_tramitacoes_for_camara_project
from .fetcher import fetch_senado_project, fetch_tramitacoes_for_senado_project

class ProjetoAddForm(forms.ModelForm):
    class Meta:
        model = Projeto
        fields = ('origem', 'id_site', 'tags')

        widgets = {
            'tags': forms.CheckboxSelectMultiple(),
        }

    def save(self, commit=True):
        tags = Tag.objects.filter(id__in=self.cleaned_data['tags'])

        if self.instance.origem == Projeto.CAMARA:
            fetcher = fetch_camara_project
        elif self.instance.origem == Projeto.SENADO:
            fetcher = fetch_senado_project

        self.instance = fetcher(self.instance.id_site, tags=tags, save=commit)

        return super(ProjetoAddForm, self).save(commit=commit)

    def _save_m2m(self, *args, **kwargs):
        super_ret = super(ProjetoAddForm, self)._save_m2m(*args, **kwargs)
        if self.instance.origem == Projeto.CAMARA:
            fetcher = scrape_tramitacoes_for_camara_project
        elif self.instance.origem == Projeto.SENADO:
            fetcher = fetch_tramitacoes_for_senado_project

        try:
            fetcher(self.instance)
        except:
            pass

    def clean(self):
        cleaned_data = super(ProjetoAddForm, self).clean()
        origem = cleaned_data['origem']
        id_site = cleaned_data['id_site']

        if origem == Projeto.CAMARA:
            fetcher = fetch_camara_project
        elif origem == Projeto.SENADO:
            fetcher = fetch_senado_project


        try:
            fetcher(id_site, save=False)
        except FetcherError:
            origem_full_name = "Senado" if origem == Projeto.SENADO else "Câmara"
            raise forms.ValidationError(
                "O projeto com id {} não existe em {}".format(id_site,
                    origem_full_name))
