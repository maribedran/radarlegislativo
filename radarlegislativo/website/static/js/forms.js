var submitForm = function(object) {
  $(object).closest("form").submit();
};

$(".submit-on-change").change(function(){
  submitForm($(this));
});

$(".submit-on-enter").on('keypress', function (e) {
  if (e.which === 13) { // Enter
    submitForm($(this));
  }
});